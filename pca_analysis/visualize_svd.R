
#visualize_svd
visualize_svd=function(x, pop, title=title){
  
  #get percentage of explained variance
  x.pc=x$d^2/sum(x$d^2)*100
  
  figure=plot(x, type="scores")+
    geom_point(size=4)+
    aes(col=pop)+
    scale_color_manual(values = c("#87ceebff","#ff8c00ff"), labels=c("zambia","sweden"))+
    ggtitle(title)+
    labs(color="population", x=paste0("PC1 ", round(x.pc[1],1),"%"), y=paste0("PC2 ", round(x.pc[2],1),"%"))+
    theme(plot.margin = unit(c(0.1,0.1,0.1,0.1), "cm"), panel.grid.major = element_blank(), panel.grid.minor = element_blank())+
    coord_fixed(ratio = 0.65)

  return(figure)
  
}