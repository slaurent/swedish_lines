import sys
from optparse import OptionParser, OptionGroup
from collections import defaultdict as d
import random
import gzip
#Author: Martin Kapun

#########################################################   HELP   #########################################################################
usage="\npython2.7 %prog --input input.consensus --ind 2,3,6,10 --names Sample1,Sample2,Sample6,Sample10 --N-cutoff 0.5 --output output.vcf"
parser = OptionParser(usage=usage)
group=OptionGroup(parser,
"""
H E L P:
____________
This script converts an input (--input ) in the consensus file format to the VCF file format and only condiders positions where at least one sample carries an alternative allele. Only samples in the consensus matrix in the last column of the input file which are defined by the --ind parameter (a comma-separated list of 0-based positions in the consensus column) are considered. The parameter --names is a comma-separated list with Sample names to be printed in the header of the VCF file. This list needs to correspond in length to the --ind parameter. The parameter --N-cutoff defines the maximal proportion of samples with missing data at a given site. E.g. --N-cutoff 0.5 means that a position is considered if no more than 50% of all samples have missing information, i.e. an "N".

""")
#########################################################   CODE   #########################################################################

parser.add_option("--input", dest="input", help="Input consensus file")
parser.add_option("--output", dest="o", help="output file")
parser.add_option("--ind", dest="i", help="indivduals used for the analysis")
parser.add_option("--N-cutoff", dest="u", help="Not more N's for a given site than x percent, e.g. 0.1")
parser.add_option("--names", dest="name", help="a list of names for the ")

parser.add_option_group(group)
(options, args) = parser.parse_args()

def load_data(x):
    ''' import data either from a gzipped or or uncrompessed file or from STDIN'''
    import gzip
    if x=="-":
        y=sys.stdin
    elif x.endswith(".gz"):
        y=gzip.open(x,"r")
    else:
        y=open(x,"r")
    return y

def isolate(x,ind):
    ''' get nucleotides for certain individuals'''
    nuc=""
    for i in ind:
        nuc+=x[i]
    return nuc

individual=map(int,options.i.split(","))
chromosome=options.c
cutoff=float(options.u)
#idhash=dict(zip(individual,positions))

if options.e!="NA":
    region=map(int,options.e.split(":"))

fullsnplist=[]
count=1

names=options.name.split(",")

indnames=[]
for i in individual:
    indnames.append(names[i])

for l in load_data(options.input):
    if count%10000000==0:
        print count,"positions processed"
    count+=1
    a=l.rstrip().split()
    if len(a)==4:
        C,P,R,D=a
    elif len(a)==3:
        C,P,D=a
        DX=D.replace("N","")
        if DX=="":
            R="N"
        else:
            R=DX[0]
    else:
        C,P,R,A,D,Q,T=a

    ## exclude SNPs from other chromosomes
    if chromosome!="NA" and C!=chromosome:
        print "wrong chromosome"
        continue

    if options.e!="NA":
        if int(P)<region[0] or int(P)>region[1]:
            continue

    DI=isolate(D,individual)
    #print l,DI


    ## test if less N's than expected
    if DI.count("N")/float(len(DI))>cutoff:
        #print "N's"
        continue

    ## only use positions with more than 1 allele
    if len(set(DI.replace("N","")))==1 or len(set(DI.replace("N","")))>2:
        #print "no allele"
        continue

    a,b=set(DI.replace("N",""))

    DI=DI.replace("N",".")
    if a==R:
        R=a
        A=b
        DI=DI.replace(a,"0")
        DI=DI.replace(b,"1")

    else:
        R=b
        A=a
        DI=DI.replace(b,"0")
        DI=DI.replace(a,"1")

    ## use proportional subset for calculations. set to 1 to use all
    if float(options.r)<1:
        if random.random() <= float(options.r):
            fullsnplist.append(C+"\t"+P+"\t"+DI+"\t"+R+"\t"+A)
    ## use a defined number of SNPs.
    else:

        fullsnplist.append(C+"\t"+P+"\t"+DI+"\t"+R+"\t"+A)
newlist=fullsnplist
if float(options.r)>1:
    newdict=random.sample(range(len(fullsnplist)),int(options.r))
    newfullsnps=[]
    for i in sorted(newdict):
        newfullsnps.append(fullsnplist[i])
    newlist=newfullsnps

VCF=open(options.o+".vcf","w")
VCF.write('''##fileformat=VCFv4.0
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
''')
VCF.write("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t"+"\t".join(indnames)+"\n")
pos=[]
for l in newlist:
    GT=[]
    C,P,D,R,A=l.split()
    sl=C+"\t"+P+"\t.\t"+R+"\t"+A+"\t.\tPASS\t.\tGT\t"
    for i in range(len(indnames)):
        if D[i]=="1" or D[i]=="0":
            GT.append(D[i]+"|"+D[i])
        else:
            GT.append(".|.")
    VCF.write(sl+"\t".join(GT)+"\n")
