
## Read mapping

We used the bioinformatics pipeline from Kapun _et al._ (2018) to map short reads against a combined reference consisting of the _Drosophila melanogaster_ genome v.6.04 and several other genomes of common microbial symbionts. A detailed description of the reference and all bioinformatic steps can be found in Kapun _et al._ (2018) and [here](https://github.com/capoony/DrosEU_pipeline). All additional steps and scripts which are not part of the original pipeline can be found in this description and in the scripts folder of this repository.

In brief, we first trimmed raw FASTQ reads for BQ >18 and minimum read length > 75bp with [cutadapt](https://cutadapt.readthedocs.io/en/stable/) after testing read quality with [FASTQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/). Then, we mapped the trimmed reads against the reference with [bwa](https://sourceforge.net/projects/bio-bwa/files/) and filtered for propper read pairs with MQ > 20 using [samtools](http://samtools.sourceforge.net/). After that, we sorted BAM files by reference position using [picard](https://broadinstitute.github.io/picard/) and removed PCR duplicates with [picard](https://broadinstitute.github.io/picard/). Finally, we added read group tags to BAM files using [picard](https://broadinstitute.github.io/picard/) and re-aligned around InDels using [GATK](https://software.broadinstitute.org/gatk/).

## Merging BAM files and joint SNP calling

Using [samtools](http://samtools.sourceforge.net/), we merged BAM files (in the order of the file paths in BAMlist.txt) in an MPILEUP file only retaining reads with MQ > 20. Then, we detected indel positions with a coverage >10 across all samples and marked positions 5bp up- and downstreams of the indel to avoid calling paralogous SNPs due to mis-mapping. The corresponding Python script can be found [here](https://github.com/capoony/DrosEU_pipeline).

```bash
python2.7 detect-indels.py \
--mpileup input.mpileup.gz \
--minimum-count 10 \
--mask 5 \
| gzip > /InDel-positions_10.txt.gz
```
Given that the sequenced genomes are haploid, we used a combination of heuristic parameters (allele frequency of the "true" allele >0.9, base quality >20 and coverage between 10 and 400) to identify genotypes based on the following criteria using the script _mpileup2cons.py_. Samples not fullfilling the coverage and quality criteria were considered as missing data ("N"). The output file is in consensus file format where columns 1 to 3 are the Chromosome, Position and Reference allele, respectively. Column 4 is a matrix, where each value is the consensus nucleotide at a given position for each sample in the same order as in the mpileup file. 

```bash
python2.7 mpileup2cons.py \
-m input.mpileup.gz \
--coverage-threshold 10,400 \
--cutoff 0.9 \
--base-quality-threshol 20 \
| gzip > output.consensus.gz
```

We then used the Python script _makeMask.py_ to generate FASTA-type mask files for each chromosome and sample, where positions not present in the consensus file or with missing data ("N") are encoded by a "0", whereas positions passing all quality criteria are encoded by a "1".

```bash
python2.7 makeMask.py \
--input Umea-Zambia.consensus.gz \
--output MASK \
--names SU05n,SU02n,SU75n,SU81n,SU93n,SU07n,SU94,SU08,SU21n,SU25n,SU26n,SU29,SU37n,SU58n,ZI117,ZI118N,ZI253,ZI488,ZI329,ZI373,ZI207,ZI200,ZI104,ZI472,ZI504,ZI431,ZI85,ZI91
```

Then, we converted the consensus file to VCF file format and only retained polymorphic positions (at least one sample with alternative allele) if less than 50% of all samples contained missing data.

```bash
python2.7 cons2vcf.py \
--input Umea-Zambia.consensus.gz \
--output SNP \
--ind 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27 \
--N-cutoff 0.5 \
--names SU05n,SU02n,SU75n,SU81n,SU93n,SU07n,SU94,SU08,SU21n,SU25n,SU26n,SU29,SU37n,SU58n,ZI117,ZI118N,ZI253,ZI488,ZI329,ZI373,ZI207,ZI200,ZI104,ZI472,ZI504,ZI431,ZI85,ZI91
```
Finally, we filtered the VCF file based on previously identified indels (see above) and known TE positions (identified by [RepeatMasker](http://www.repeatmasker.org/)) and further restricted the dataset to positions on the major chromosmal arms. The corresponding python script can be found [here](https://github.com/capoony/DrosEU_pipeline).

```bash
python2.7 FilterPosFromVCF.py \
--indel InDel-positions_10.txt.gz \
--te RM_dmel-all-chromosome-r6.10.fasta.out.gff \
--vcf SNPs.vcf.gz \
| awk '$1~"#" || $1=="X" || $1=="2L" || $1=="2R" || $1=="3L" || $1=="3R" || $1=="4"' \
| gzip > SNPs_clean.vcf.gz
```

## Inversion frequencies

We indirectly assessed inversion frequencies of _In(2L)t, In(2R)NS, In(3L)P, In(3R)C, In(3R)Mo_ and _In(3R)Payne_ based on inversion-specific alleles (inversion_markers_v6.txt) at multiple loci, which were previously characterized in Kapun _et al._ (2014).

```bash
python2.7 cons2inv.py \
--inv-markers inversion_markers_v6.txt \
--input Umea-Zambia.consensus.gz \
--names SU05n,SU02n,SU75n,SU81n,SU93n,SU07n,SU94,SU08,SU21n,SU25n,SU26n,SU29,SU37n,SU58n,ZI117,ZI118N,ZI253,ZI488,ZI329,ZI373,ZI207,ZI200,ZI104,ZI472,ZI504,ZI431,ZI85,ZI91
> Umea-Zambia.inv
```

## References

Kapun M, van Schalkwyk H, Bryant M, Flatt T, Schlötterer C. 2014. Inference of chromosomal inversion dynamics from Pool-Seq data in natural and laboratory populations of _Drosophila melanogaster_. Molecular Ecology 23:1813–1827.

Kapun M, Barron Aduriz MG, Staubach F, Vieira J, Obbard D, Goubert C, Rota Stabelli O, Kankare M, Haudry A, Wiberg RAW, et al. 2018. Genomic analysis of European _Drosophila_ populations reveals longitudinal structure and continent-wide selection. bioRxiv Available from: http://biorxiv.org/lookup/doi/10.1101/313759

