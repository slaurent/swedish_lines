import sys
from collections import defaultdict as d
import gzip
import os
from optparse import OptionParser, OptionGroup

#Author: Martin Kapun

#########################################################   HELP   #########################################################################
usage="\npython2.7 %prog --input input.consensus --names Sample1,Sample2,Sample6,Sample10 --output MASK"
parser = OptionParser(usage=usage)
group=OptionGroup(parser,
"""
H E L P:
____________
Based on an input in consensus file format (--input), this script generates FASTA-type mask files for each chromosome and sample, where positions not present in the consensus file or with missing data ("N") are encoded by a "0", whereas positions passing all quality parameters are encoded by a "1".

""")
#########################################################   CODE   #########################################################################

parser.add_option("--input", dest="input", help="Input consensus file")
parser.add_option("--output", dest="o", help="output file prefix")
parser.add_option("--names", dest="name", help="a list of names corresponding to the samples in the consensus file.")

parser.add_option_group(group)
(options, args) = parser.parse_args()


def load_data(x):
	''' import data either from a gzipped or or uncrompessed file or from STDIN'''
	import gzip
	if x=="-":
		y=sys.stdin
	elif x.endswith(".gz"):
		y=gzip.open(x,"r")
	else:
		y=open(x,"r")
	return y



names=options.name.split(",")
code={"2L":23513712,"2R":25286936,"3L":28110227,"3R":32079331,"X":23542271,"4":32079331,"Y":3667352}
Ch=""
nuch=d(list)
tot=d(lambda:d(str))
T=d(int)

for l in load_data(options.input):
    C,P,R,Cons=l.rstrip().split()
    if C not in code:
        continue
    if int(P)%1000000==0:
        print C,P
        #print nuch
    ## test if variable Ch is empty = beginning of file
    if Ch=="":
        Ch=C
        ## test if position starts with 1 otherwise, fill position from 1 to P with 0's
        if int(P)!=1:
            print P,"larger than 1 in",C
            Start=1
            while Start<int(P):
                for i in range(len(Cons)):
                    nuch[names[i]].append("0")
                Start+=1
        ## fill hash for first position in file
        for i in range(len(Cons)):
            if Cons[i]=="N":
                nuch[names[i]].append("0")
            else:
                nuch[names[i]].append("1")
                T[names[i]]+=1
        ## Store Postition to compare to next
        Ppr=int(P)
        continue
    ## test if Chromosome is different from Chrom in previous row
    elif Ch!=C:
        ## test if last position for chromosome is identical with Chromsomes length, if not fill 0's until end
        if Ppr<code[Ch]:
            print Ppr,"smaller than maximum for ",Ch,",which is ",code[Ch]
            Ppr+=1
            while Ppr<=code[Ch]:
                print "now it's ",Ppr,", should be",code[Ch]
                for i in range(len(Cons)):
                    nuch[names[i]].append("0")
                Ppr+=1
        ## write mask code to new files for each library
        for k,v in nuch.items():
            if not os.path.exists(options.o+"/"+k):
                os.makedirs(options.o+"/"+k)
            out=gzip.open(options.o+"/"+k+"/mask_"+k+"_"+Ch+".fasta.gz","w")
            out.write(">"+k+"_"+Ch+"\n")
            out.write("".join(v)+"\n")
            out.close()
            tot[k][Ch]=T[k]
        print Ch,"processed"

        ## update chromosome and hash
        Ch=C
        nuch=d(list)

        ## test if position starts with 1 otherwise, fill position from 1 to P with 0's
        if int(P)!=1:
            print P,"larger than 1 in",C
            Start=1
            while Start<int(P):
                for i in range(len(Cons)):
                    nuch[names[i]].append("0")
                Start+=1

        ## fill hash for first position of new chromsome
        for i in range(len(Cons)):
            if Cons[i]=="N":
                nuch[names[i]].append("0")
            else:
                nuch[names[i]].append("1")
                T[names[i]]+=1
        Ppr=int(P)
        continue

    ## test if current position seamlessly continues with previous position, otherwise, fill positions from previous position to P with 0's
    if Ppr!=int(P)-1:
        print Ppr,"is not",int(P)-1,"for",C
        Ppr+=1
        while Ppr<int(P):
            print "now it's ",Ppr,", should be",P
            for i in range(len(Cons)):
                nuch[names[i]].append("0")
            Ppr+=1
    ## fill hash for first position of new chromsome
    for i in range(len(Cons)):
        if Cons[i]=="N":
            nuch[names[i]].append("0")
        else:
            nuch[names[i]].append("1")
            T[names[i]]+=1
    Ppr=int(P)
 ## test if last position for chromosome is identical with Chromsomes length, if not fill 0's until end
if Ppr<code[Ch]:
    print Ppr,"smaller than maximum for ",Ch,",which is ",code[Ch]
    Ppr+=1
    while Ppr<=code[Ch]:
        print "now it's ",Ppr,", should be",code[Ch]
        for i in range(len(Cons)):
            nuch[names[i]].append("0")
        Ppr+=1

 ## write mask code to new files for each library
for k,v in nuch.items():
    if not os.path.exists(options.o+"/"+k):
        os.makedirs(options.o+"/"+k)
    out=gzip.open(options.o+"/"+k+"/mask_"+k+"_"+Ch+".fasta.gz","w")
    out.write(">"+k+"_"+Ch+"\n")
    out.write("".join(v)+"\n")
    out.close()
    tot[k][Ch]=T[k]
print Ch,"processed"
out=open(options.o+"/sites.txt","w")

H=0
for k,v in sorted(tot.items()):
    if H==0:
        out.write("Line\t"+"\t".join(sorted(v.keys()))+"\n")
        H=1
    tl=k
    for C,T in sorted(v.items()):
        tl+="\t"+str(T)
    out.write(tl+"\n")
