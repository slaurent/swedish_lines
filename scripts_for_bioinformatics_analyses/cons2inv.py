import sys
from collections import defaultdict as d
from optparse import OptionParser, OptionGroup

#Author: Martin Kapun

#########################################################   HELP   #########################################################################
usage="\npython2.7 %prog --input input.consensus --names Sample1,Sample2,Sample6,Sample10 --inv-markers inversion_markers_v6.txt > output.inv"
parser = OptionParser(usage=usage)
group=OptionGroup(parser,
"""
H E L P:
____________

This script calculates population-specific inversions frequencies based on inversion-specific marker SNPs in Kapun et al. (2014)
""")
#########################################################   CODE   #########################################################################

parser.add_option("--input", dest="input", help="Input consensus file")
parser.add_option("--inv-markers", dest="inv", help="List of inversion-specific alleles, see inversion_markers_v6.txt")
parser.add_option("--names", dest="name", help="a list of names corresponding to the samples in the consensus file.")

parser.add_option_group(group)
(options, args) = parser.parse_args()


def load_data(x):
	''' import data either from a gzipped or or uncrompessed file or from STDIN'''
	import gzip
	if x=="-":
		y=sys.stdin
	elif x.endswith(".gz"):
		y=gzip.open(x,"r")
	else:
		y=open(x,"r")
	return y

names=options.name.split(",")
invhash=d(tuple)
for l in open(options.input):
    if l.startswith("inversion"):
        continue
    I,C,P,A=l.rstrip().split()
    invhash[C+P]=(I,A)

invlist=d(lambda:d(list))
for l in load_data(options.inv):
    C,P,Allele,Cons=l.rstrip().split()
    if C+P not in invhash:
        continue
    I,A=invhash[C+P]
    for i in range(len(Cons)):
        if Cons[i]=="N":
            invlist[names[i]][I]
            continue
        elif Cons[i]==A:
            invlist[names[i]][I].append(1)
        else:
            invlist[names[i]][I].append(0)

H=0
for k,v in sorted(invlist.items()):
    if H==0:
        print "Pop\t"+"\t".join(sorted(v.keys()))
        H=1
    Freql=k
    for I,F in sorted(v.items()):
        if len(F)==0:
            Freql+="\tNA"
        else:
            #Freql+="\t"+str(F.count(1))+"/"+str(F.count(0))
            Freql+="\t"+str(sum(F)/float(len(F)))
    print Freql
