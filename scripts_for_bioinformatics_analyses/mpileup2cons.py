import sys
import collections
import re
from optparse import OptionParser, OptionGroup

#Author: Martin Kapun

#########################################################   HELP   #########################################################################
usage="\npython2.7 %prog --mpileup input.mpileup --coverage-threshold 10,400 --base-quality-threshold 20 --cutoff 0.9 --output outputfolder/updated_genomes"
parser = OptionParser(usage=usage)
helptext="""

H E L P:
_________

This script uses an mpileup file (--mpileup) of haploid samples as the input and identifies the "true" allele if the coverage is within a given range (--coverage-threshold min,max), the base-quality is above a threshold (--basequality) and the frequency of the "true" allele is equal or greater than a threshold value (--cutoff). If a given sample does not pass these criteria, this position will be considered missing/ambiguous ("N").

The output is in the consensus file format where columns 1 to 3 are the Chromosome, Position and Reference allele, respectively. Column 4 is a matrix, where each value is the consensus nucleotide at a given position for all samples in the same order as in the mpileup input file.

"""
group=OptionGroup(parser,helptext)
#########################################################   parameters   #########################################################################

parser.add_option("-m","--mpileup", dest="m", help="A mpileup file")
parser.add_option("-c","--coverage-threshold", dest="c", help="a list containing the minimum and the maximum coverage thresholds separated by a comma: e.g. 10,80")
parser.add_option("-b","--base-quality-threshold", dest="b", help="The Base-quality threshold for Qualities encoded in Sanger format (Illumina 1.8 format)")
parser.add_option("-u","--cutoff", dest="u", help="the threshold frequency of the 'true' allele, everything smaller will be considered an ambigous site = missing data")

parser.add_option_group(group)
(options, args) = parser.parse_args()

###################################### functions #############################################

def keywithmaxvalue(d):
    ''' This function resturns the key for the maximum value in a dictionary'''
    newhash=collections.defaultdict(list)
    for k,v in d.items():
        newhash[v].append(k)
    return newhash[max(newhash.keys())]


def splitter(l, n):
    ''' This generator function returns equally sized cunks of an list'''
    #credit: Meric Lieberman, 2012
    i = 0
    chunk = l[:n]
    while chunk:
        yield chunk
        i += n
        chunk = l[i:i+n]

def extract_indel(l,sign):
    ''' This function returns an Indel from a sequence string in a pileup'''
    position = l.index(sign)
    numb =""
    i = 0
    while True:
        if l[position+1+i].isdigit():
            numb+= l[position+1+i]
            i+=1
        else:
            break

    seqlength = int(numb)
    sequence = l[position:position+i+1+seqlength]
    indel=sequence.replace(numb,"")

    return sequence,indel

def load_data(x):
	import gzip
	if x=="-":
		y=sys.stdin
	elif x.endswith(".gz"):
		y=gzip.open(x,"r")
	else:
		y=open(x,"r")
	return y


##################################### data #################################################

data=load_data(options.m)
threshold=int(options.b)
cutoff=float(options.u)

if len(map(int,options.c.split(",")))>2:
    covmin=map(int,options.c.split(","))[0]
    covmax=map(int,options.c.split(","))[1:]
else:
    covmin,covmax=map(int,options.c.split(","))

##################################### main code #################################################

# parse mpileup and store alternative alleles:
allelehash=collections.defaultdict(lambda:collections.defaultdict(lambda:collections.defaultdict(str)))
for line in data:
    #print line
    k = line[:-1].split('\t')
    chrom,position,refbase = k[:3]
    div = list(splitter(k,3))
    libraries=div[1:]

    cons=[]
    # loop through libraries
    for j in range(len(libraries)):

        nuc = libraries[j][1]
        qualities = libraries[j][2]
        alleles=collections.defaultdict(int)

        # test if seq-string is empty
        if nuc=="*":
            cons.append("N")
            continue

        # test whether position within coverage thresholds
        cov = libraries[j][0]
        if isinstance(covmax, int):
            if int(cov)<covmin or int(cov)>covmax:
                cons.append("N")
                continue
        else:
            if int(cov)<covmin or int(cov)>covmax[j]:
                cons.append("N")
                continue

        # find and remove read indices and mapping quality string
        nuc = re.sub(r'\^.',r'',nuc)
        nuc = nuc.replace('$','')

        # find and remove InDels
        while "+" in nuc or "-" in nuc:
            if "+" in nuc:
                insertion,ins=extract_indel(nuc,"+")
                #alleles[ins.upper()]+=nuc.count(insertion)
                nuc=nuc.replace(insertion,"")
            else:
                deletion,dele=extract_indel(nuc,"-")
                #alleles[dele.upper()]+=nuc.count(deletion)
                nuc=nuc.replace(deletion,"")

        # read all alleles
        for i in range(len(nuc)):

            # test for base quality threshold (if below: ignore nucleotide)
            if ord(qualities[i])-33<threshold:
                continue
            # ignore single nucleotide deletions
            if nuc[i]=="*":
                continue
            # count nucleotides similar to reference base
            if nuc[i] =="," or nuc[i] == ".":
                alleles[refbase]+=1
                continue
            # count altenrative nucleotides
            alleles[nuc[i].upper()]+=1

        # ignore position if coverage after filtering for 1) InDels and base-quality below threshold
        if sum(alleles.values())<covmin:
            cons.append("N")
            continue

        af={}
        for k,v in alleles.items():
            af[v/float(sum(alleles.values()))]=k

        if max(af.keys())>cutoff:
            cons.append(af[max(af.keys())])
        else:
            cons.append("N")
    print chrom+"\t"+position+"\t"+refbase+"\t"+"".join(cons)
