#!/usr/binenv python 

import numpy
import dadi
import demographic_model_nomig

data = dadi.Spectrum.from_file('Pop1_Pop2_input_for_dadi.fs')
ns = data.sample_sizes
pts_l = [40,50,60]

func = demographic_model_nomig.nomig
paramlist = ["time growth","Popsize2","bottleneck Popsize1","Popsize1","time split"]

upper_bound = [20, 200, 20, 200, 5] # TAf, nuAf, nuEu0, nuEu, TEuAf
lower_bound = [0, 1e-3, 1e-4, 1e-3, 0]
p0 = [1, 1, 0.1, 1, 1] # 1st run
# p0 = [0.3, 4, 0.1, 0.8, 0.1] # 2nd run
# p0 = [0.5, 4, 0.04, 7, 0.09] # 3rd run
# p0 = [4, 10, 0.03, 3, 0.06] # 4th run
# p0 = [0.5, 10, 5, 10, 4] # 5th run
# p0 = [0.2, 2.6, 0.02, 2.5, 0.05] # 6th run <-----
# p0 = [2, 10, 1, 10, 1] # 7th run

func_ex = dadi.Numerics.make_extrap_log_func(func)
p0 = dadi.Misc.perturb_params(p0, fold=1, upper_bound=upper_bound, lower_bound=lower_bound)
popt = dadi.Inference.optimize_log_fmin(p0, data, func_ex, pts_l, lower_bound=lower_bound, upper_bound=upper_bound, verbose=len(p0), maxiter=800)

model = func_ex(popt, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Maximum log composite likelihood: {0}'.format(ll_model))
theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))
