"""
Demographic model_asym_mig: Size change in the ancestral population, bottleneck for population 1 and then exponential growth
"""
import numpy
import dadi
from dadi import Numerics, PhiManip, Integration, Spectrum

def asym_mig((TAf, nuAf, nuEu0, nuEu, TEuAf, m12, m21), ns, pts):
    
    """
    TAf: Time between growth and split
    nuAf: Ancestral population size after growth
    nuEu0: Bottleneck size for pop1
    nuEu: Final size for pop1 (after bottleneck)
    TEuAf: Time between split and present
    m12: Scaled migration rate from Eu to Af
    m21: Scaled migration rate from Af to Eu
    """
    
    # Define the grid we'll use: always the 1st step
    xx = Numerics.default_grid(pts) # pts = number of grid points in each dimension for representing phi ( = continuous density of mutations, approximated by its values on a grid of points)

    # All demographic models must begin with an equilibrium pop of non-0 size
    phi = PhiManip.phi_1D(xx) # phi for the equilibrium ancestral population, nu specifies the relative size of this ancestral pop to the ref pop. Here ref pop = ancestral, so nu = 1 by default
    
    # Pop growth event
    phi = Integration.one_pop(phi, xx, TAf, nu=nuAf)

    # Once we've created an initial phi, we can begin to manipulate it
    phi = PhiManip.phi_1D_to_2D(xx, phi) # The divergence: split phi to simulate pop splits: take in an input phi of 1D and output a phi of 1 greater D, corresponding to addition of a pop
    
    # Bottleneck for pop1
    nuEu_funct = lambda t: nuEu0*(nuEu/nuEu0)**(t/TEuAf)
    
    # Recovery
    phi = Integration.two_pops(phi, xx, TEuAf, nu1=nuEu_funct, nu2=nuAf, m12=m12, m21=m21)
    
    # Finally, calculate the spectrum
    fs = Spectrum.from_phi(phi, ns, (xx, xx))
    return fs



