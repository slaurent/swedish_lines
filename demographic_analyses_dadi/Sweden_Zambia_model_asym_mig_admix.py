#!/usr/binenv python 

import numpy
import dadi
import demographic_model_asym_mig_admix

data = dadi.Spectrum.from_file('Pop1_Pop2_input_for_dadi.fs')
ns = data.sample_sizes
pts_l = [40,50,60]

func = demographic_model_asym_mig_admix.asym_mig_admix
paramlist = ["time growth","Popsize2","bottleneck Popsize1","Popsize1","time split", "migration 12", "migration 21", "time admixture"]

upper_bound = [10, 10, 50, 100, 20, 5, 10, 10] # TAf, nuAf, nuEu0, nuEu, TEuAf, m12, m21, TM
lower_bound = [0, 1e-3, 1e-5, 1e-3, 0, 0, 0, 0]
p0 = [1, 1, 0.1, 1, 1, 1, 1, 1] # 1st run
# p0 = [0.0007, 4, 0.09, 0.1, 0.1, 2, 0.5, 0.5] # 2nd run
# p0 = [0.0007, 4, 0.1, 0.7, 0.02, 0.9, 0.9, 1] # 3rd run
# p0 = [0.0007, 4, 0.7, 0.7, 0.02, 0.9, 0.9, 1] # 4th run
# p0 = [0.00000000002, 4, 0.3, 10, 0.02, 0.9, 0.9, 1] # 5th run
# p0 = [4, 4, 5, 10, 4, 0.9, 0.9, 1] # 6th run
# p0 = [4, 4, 5, 10, 4, 3, 0.9, 1] # 7th run
# p0 = [0.00000000002, 4, 0.3, 0.4, 4, 3, 5, 1] # 8th run
# p0 = [0.00000000002, 4, 0.3, 0.4, 0.2, 4, 5, 4] # 9th run
# p0 = [0.00000000002, 4, 0.1, 10, 4, 3, 5, 4] # 10th run
# p0 = [0.00000000002, 5, 0.1, 1, 4, 3, 5, 4] # 11th run
# p0 = [0.00000000002, 4, 0.1, 1, 2.5, 3, 5, 4] # 12th run
# p0 = [0.00000000002, 4, 0.3, 0.4, 0.2, 2, 0.6, 0.2] # 13th run <----------------
# p0 = [0.1, 5, 0.1, 1, 2.5, 1, 0.4, 4] # 14th run
# p0 = [0.1, 5, 0.1, 1, 2.5, 1, 0.4, 2] # 15th run

func_ex = dadi.Numerics.make_extrap_log_func(func)
p0 = dadi.Misc.perturb_params(p0, fold=1, upper_bound=upper_bound, lower_bound=lower_bound)
popt = dadi.Inference.optimize_log_fmin(p0, data, func_ex, pts_l, lower_bound=lower_bound, upper_bound=upper_bound, verbose=len(p0), maxiter=800)

model = func_ex(popt, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Maximum log composite likelihood: {0}'.format(ll_model))
theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))

