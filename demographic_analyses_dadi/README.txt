1 - vcf2dadiInput.sh for autosomes and chrX separately
2 - dadi_snp2sfs.py for autosomes and chrX separately
3 - create demographic_model_*.py
4 - run Sweden_Zambia_model_*.py for all models and for autosomes and chrX separately
5 - simulations_model_asym_mig.py for autosomes and chrX separately
6 - fitting_model_asym_mig.py for autosomes and chrX separately, to calculate CI