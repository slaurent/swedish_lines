"""
	Python script to fit the asym_mig model to simulated data.
	Requires mydemos.py in the working directory to define demographic models.
"""
import numpy
import dadi
from time import time
import os

T1 = time()

for root, dirs, files in os.walk("sims"):
    for names in files:
        filename=os.path.join(root,names)
        PODS= dadi.Spectrum.from_file(filename)

        nsam = PODS._get_sample_sizes()
        sample_sizes = [nsam[0],nsam[1]]
        pts_l = [40, 50, 60]

        import demographic_model_asym_mig

        ISOmod = demographic_model_asym_mig.asym_mig
        ISO_ex = dadi.Numerics.make_extrap_log_func(ISOmod)

        upperISO = [10, 100, 10, 100, 5, 10, 10]
        lowerISO = [0, 1e-3, 1e-3, 1e-4, 0, 0, 0]
        startISO = [0.15, 2, 0.05, 0.5, 0.1, 2, 0.5]
        p0ISO = dadi.Misc.perturb_params(startISO, fold=1, upper_bound=upperISO, lower_bound=lowerISO)

        poptISO = dadi.Inference.optimize_log_fmin(p0ISO, PODS, ISO_ex, pts_l, lower_bound = lowerISO, upper_bound = upperISO, verbose=False, maxiter=800)

        ISOmod = ISO_ex(poptISO, sample_sizes, pts_l)
        thetaISO = dadi.Inference.optimal_sfs_scaling(ISOmod, PODS)
        ISOmod *= thetaISO
        llISO = dadi.Inference.ll(ISOmod, PODS)
        T2 = time()

        print llISO, thetaISO, poptISO[0], poptISO[1], poptISO[2], poptISO[3], poptISO[4], poptISO[5], poptISO[6]
