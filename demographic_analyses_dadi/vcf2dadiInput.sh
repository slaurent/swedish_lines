vcftools --vcf Pop1.vcf --counts --out Pop1
# replace : with tab in the Pop1.frq.count file, add & modify header
sort -u Pop1.frq.count > Pop1.frq.count_uniq
R
Pop1 = read.table("Pop1.frq.count_uniq", header=TRUE, stringsAsFactors=FALSE)
Pop2 = read.table("Pop2.frq.count_uniq", header=TRUE, stringsAsFactors=FALSE)
both_pop = merge(Pop1, Pop2, by="CHROM_POS")
write.table(both_pop, file="Pop1_Pop2.frq.count_uniq.txt",quote=FALSE, sep="\t", row.names=FALSE, col.names=TRUE)
quit()
# change header and replace _ with tab
awk '{print "-"$5"-\t---\t"$5"\t"$6"\t"$12"\t"$7"\t"$8"\t"$14"\t"$1"\t"$2}' Pop1_Pop2.frq.count_uniq.txt > Pop1_Pop2_input_for_dadi.txt
# change header
source activate py27  # Dadi works only on py27
dadi_snp2sfs.py