#!/usr/binenv python 

import numpy
import dadi
import demographic_model_asym_mig

data = dadi.Spectrum.from_file('Pop1_Pop2_input_for_dadi.fs')
ns = data.sample_sizes
pts_l = [40,50,60]

func = demographic_model_asym_mig.asym_mig
paramlist = ["time growth","Popsize2","bottleneck Popsize1","Popsize1","time split", "migration 12", "migration 21"]

upper_bound = [10, 100, 10, 100, 5, 10, 10] # TAf, nuAf, nuEu0, nuEu, TEuAf, m12, m21
lower_bound = [0, 1e-3, 1e-3, 1e-4, 0, 0, 0]
p0 = [1, 1, 0.1, 1, 1, 1, 1] # 1st run
# p0 = [0.1, 2.9, 0.1, 0.3, 2.2, 2.2, 0.7] # 2nd run
# p0 = [4, 2.7, 0.2, 0.4, 0.7, 2, 0.7] # 3rd run
# p0 = [4, 10, 0.2, 0.4, 0.7, 2, 0.7] # 4th run
# p0 = [4, 10, 5, 0.4, 0.7, 2, 0.7] # 5th run
# p0 = [4, 10, 5, 10, 0.7, 2, 0.7] # 6th run
# p0 = [0.45, 4, 5, 10, 4, 2, 0.7] # 7th run
# p0 = [4, 10, 5, 10, 4, 5, 0.7] # 8th run
# p0 = [0.45, 4, 0.07, 1, 4, 5, 5] # 9th run
# p0 = [0.15, 2, 5, 10, 4, 5, 5] # 10th run
# p0 = [0.3, 4, 0.1, 0.7, 0.2, 5, 5] # 11th run
# p0 = [0.15, 2, 0.05, 0.5, 4, 5, 5] # 12th run
# p0 = [0.3, 4, 0.1, 0.7, 0.2, 1.3, 5] # 13th run
# p0 = [0.15, 2, 0.05, 0.5, 0.1, 2, 5] # 14th run
# p0 = [0.15, 2, 0.05, 0.5, 0.1, 2, 0.5] # 15th run <-----------------
# p0 = [0.18, 2, 0.04, 0.5, 0.1, 2, 0.4] # 16th run
# p0 = [0.15, 2, 0.05, 0.5, 0.1, 2, 0.5] # repeat for the plots

func_ex = dadi.Numerics.make_extrap_log_func(func)
p0 = dadi.Misc.perturb_params(p0, fold=1, upper_bound=upper_bound, lower_bound=lower_bound)
popt = dadi.Inference.optimize_log_fmin(p0, data, func_ex, pts_l, lower_bound=lower_bound, upper_bound=upper_bound, verbose=len(p0), maxiter=800)

model = func_ex(popt, ns, pts_l)
ll_model = dadi.Inference.ll_multinom(model, data)
print('Maximum log composite likelihood: {0}'.format(ll_model))

theta = dadi.Inference.optimal_sfs_scaling(model, data)
print('Optimal value of theta: {0}'.format(theta))

# Results for best model
popt_lonrun = [0.376088, 4.08268, 0.0891882, 0.875146, 0.175048, 1.22811, 0.401923]
model_sweden = model.marginalize([1])
model_zambia = model.marginalize([0])
model_sweden_folded = model_sweden.fold()
model_zambia_folded = model_zambia.fold()

data_sweden = data.marginalize([1])
data_zambia = data.marginalize([0])

pi_sweden = model_sweden_folded.pi()
tajD_sweden = model_sweden_folded.Tajima_D()
fst = model.Fst()
pi_zambia = model_zambia_folded.pi()
tajD_zambia = model_zambia_folded.Tajima_D()
data_zambia.Tajima_D()

# Plot a comparison of the resulting fs with the data.
import pylab
pylab.figure(1)
dadi.Plotting.plot_2d_comp_multinom(model, data, vmin=1, resid_range=3, pop_ids =('Sweden','Zambia')) # plot comparisons between 2D models & data
# This ensures that the figure pops up. It may be unecessary if you are using
# ipython.
pylab.show()

