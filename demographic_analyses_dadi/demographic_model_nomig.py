"""
Demographic model_nomig: Size change in the ancestral population, bottleneck for population 1 and then gradual growth
"""
import numpy
import dadi
from dadi import Numerics, PhiManip, Integration, Spectrum

def nomig((TAf, nuAf, nuEu0, nuEu, TEuAf), ns, pts):
    
    """
    TAf: Time between growth and split
    nuAf: Ancestral population size after growth
    nuEu0: Bottleneck size for pop1
    nuEu: Final size for pop1 (after bottleneck)
    TEuAf: Time between split and present
    """
    
    # Define the grid we'll use: always the 1st step
    xx = Numerics.default_grid(pts) # pts = number of grid points in each dimension for representing phi ( = continuous density of mutations, approximated by its values on a grid of points)

    # All demographic models must begin with an equilibrium pop of non-0 size
    phi = PhiManip.phi_1D(xx) # phi for the equilibrium ancestral population, nu specifies the relative size of this ancestral pop to the ref pop. Here ref pop = ancestral, so nu = 1 by default
    
    # Pop growth event: instantaneous
    phi = Integration.one_pop(phi, xx, TAf, nu=nuAf)

    # Once we've created an initial phi, we can begin to manipulate it
    phi = PhiManip.phi_1D_to_2D(xx, phi) # The divergence: split phi to simulate pop splits: take in an input phi of 1D and output a phi of 1 greater D, corresponding to addition of a pop
    
    # Bottleneck for pop1: instantaneous size change
    nuEu_funct = lambda t: nuEu0*(nuEu/nuEu0)**(t/TEuAf)
    
    # Gradual Recovery
    phi = Integration.two_pops(phi, xx, TEuAf, nu1=nuEu_funct, nu2=nuAf)
    
    # Finally, calculate the spectrum
    fs = Spectrum.from_phi(phi, ns, (xx, xx))
    return fs

