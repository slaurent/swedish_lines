"""
    This script simulates the joint AFS, 
    Two epoch model with split, bottleneck and asym mig
"""

import numpy
import dadi

sample_sizes = [13, 13]

pts_l = [40, 50, 60]

print sample_sizes
print pts_l

simparms = [14860.0077028, 0.376088, 4.08268, 0.0891882, 0.875146, 0.175048, 1.22811, 0.401923]

truemod = "ISO"

import demographic_model_asym_mig
ISOmod = demographic_model_asym_mig.asym_mig
ISO_ex = dadi.Numerics.make_extrap_log_func(ISOmod)

for x in range(1,1001):
    truedata = simparms[0]*ISO_ex(simparms[1:], sample_sizes, pts_l)
    PODS = truedata.sample() # Sample from SFS to generate "data set"
    PODSfold = PODS.fold()
    name = "ISObPOD-%d-%d" % (sample_sizes[0], x)
    PODSfold.to_file(name)

