#!/usr/binenv python
import dadi
from numpy import array

snp_data = dadi.Misc.make_data_dict("Pop1_Pop2_input_for_dadi.txt")

fs = dadi.Spectrum.from_data_dict(snp_data, pop_ids=['Pop1','Pop2'], projections=[14,14], polarized=False) # The resulting object fs (Spectrum) is a 2D matrix

fs.to_file("Pop1_Pop2_input_for_dadi.fs")

import matplotlib.pyplot as pyplot

# Plot JSFS

pyplot.figure() # Open a window that will host our next figure. If not alias: matplotlib.pyplot.figure()

dadi.Plotting.plot_single_2d_sfs(fs, vmin=1) # Creates graphical representation of the JSFS, vmin ncs otherwise vmin = minus infinity

pyplot.show() # To make the figure appear

fs_Pop1 = fs.marginalize([1]) # Pop to be marginalized (excluded)
fs_Pop1.to_file("Pop1_input_for_dadi.fs")

fs_Pop2 = fs.marginalize([0])
fs_Pop2.to_file("Pop2_input_for_dadi.fs")

# Plot SFS

pyplot.figure()

dadi.Plotting.plot_1d_fs(fs_Pop1)

pyplot.show()

pyplot.figure()

dadi.Plotting.plot_1d_fs(fs_Pop2)

pyplot.show()

# Calculate some parameters, need to be normalised by number of sites:
pi_Pop1 = fs_Pop1.pi()
tajD_Pop1 = fs_Pop1.Tajima_D()
fst = fs.Fst()
pi_Pop2 = fs_Pop2.pi()
tajD_Pop2 = fs_Pop2.Tajima_D()
thetaW_Pop1 = fs_Pop1.Watterson_theta()
thetaW_Pop2 = fs_Pop2.Watterson_theta()


