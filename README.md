
# Bioinformatics and statistics scripts for Kapopoulou, Kapun et al (2020)

## scripts for bioinformatics analyses (Martin Kapun - martin.kapun@ieu.uzh.ch)
This folder contains all scripts used to process the vcf files starting from the short-read accessions. 

## pca analysis
This folder contains the code used to generate the PCA in Figure 1

## demographic analyses with dadi (Mado Kapopoulou adamandia@gmail.com)
all scripts to process the data, implement the demographic models, and calculate the confidence intervals. 


